EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7675 2975 8225 2975
Text Label 8225 2975 2    50   ~ 0
IO21
Wire Wire Line
	7675 3075 8225 3075
Text Label 8225 3075 2    50   ~ 0
IO17-TX
Wire Wire Line
	7675 3175 8225 3175
Text Label 8225 3175 2    50   ~ 0
IO16-RX
Wire Wire Line
	7675 3275 8225 3275
Text Label 8225 3275 2    50   ~ 0
IO19-MISO
Wire Wire Line
	7675 3375 8225 3375
Text Label 8225 3375 2    50   ~ 0
IO18-MOSI
Wire Wire Line
	7675 3475 8225 3475
Text Label 8225 3475 2    50   ~ 0
IO5-SCK
Wire Wire Line
	7675 3575 8225 3575
Text Label 8225 3575 2    50   ~ 0
IO4-A5-T2-CS
Wire Wire Line
	7675 3675 8225 3675
Text Label 8225 3675 2    50   ~ 0
I36-A4
Wire Wire Line
	7675 3775 8225 3775
Text Label 8225 3775 2    50   ~ 0
I39-A3
Wire Wire Line
	7675 3875 8225 3875
Text Label 8225 3875 2    50   ~ 0
I34-A2
Wire Wire Line
	7675 3975 8225 3975
Text Label 8225 3975 2    50   ~ 0
IO25-A1
Wire Wire Line
	7675 4075 8225 4075
Text Label 8225 4075 2    50   ~ 0
IO26-A0
Wire Wire Line
	7675 4175 8225 4175
Text Label 8225 4175 2    50   ~ 0
GND
Wire Wire Line
	6650 3300 6075 3300
Text Label 6075 3300 0    50   ~ 0
IO32-A7-T1
Wire Wire Line
	6650 3200 6075 3200
Text Label 6075 3200 0    50   ~ 0
IO14-A6-T2
Wire Wire Line
	6650 3100 6075 3100
Text Label 6075 3100 0    50   ~ 0
IO22-SCL
Wire Wire Line
	6650 3000 6075 3000
Text Label 6075 3000 0    50   ~ 0
IO23-SDA
Wire Wire Line
	6650 3600 6075 3600
Text Label 6075 3600 0    50   ~ 0
IO27-A10-T2
Wire Wire Line
	6650 3500 6075 3500
Text Label 6075 3500 0    50   ~ 0
IO33-A9-T1
Wire Wire Line
	6650 3400 6075 3400
Text Label 6075 3400 0    50   ~ 0
IO15-A8-T2
Wire Wire Line
	6650 3800 6075 3800
Text Label 6075 3800 0    50   ~ 0
IO13-A12-T2
NoConn ~ 7675 4275
$Comp
L Connector:Conn_01x12_Female J2
U 1 1 5F7E1870
P 6850 3500
F 0 "J2" H 6500 4250 50  0000 L CNN
F 1 "Adafruit_ESP32-L_1x12" H 6175 4150 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical_SMD_Pin1Right" H 6850 3500 50  0001 C CNN
F 3 "~" H 6850 3500 50  0001 C CNN
	1    6850 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x16_Female J1
U 1 1 5F7D454E
P 7475 3675
F 0 "J1" H 7225 4600 50  0000 C CNN
F 1 "Adafruit_ESP32-R_1x16" H 7200 4500 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x16_P2.54mm_Vertical_SMD_Pin1Right" H 7475 3675 50  0001 C CNN
F 3 "~" H 7475 3675 50  0001 C CNN
	1    7475 3675
	-1   0    0    -1  
$EndComp
Text Label 8225 4375 2    50   ~ 0
3V3-O250mA
Wire Wire Line
	7675 4375 8225 4375
Wire Wire Line
	6650 4100 6075 4100
Text Label 6075 4100 0    50   ~ 0
ESP32-BAT
Wire Wire Line
	6650 3900 6075 3900
Text Label 6075 3900 0    50   ~ 0
USB
Text Label 4600 4425 0    50   ~ 0
Motor-BAT
Wire Wire Line
	4950 4425 4600 4425
Text Label 4600 4325 0    50   ~ 0
GND
Wire Wire Line
	5400 4325 4600 4325
NoConn ~ 6650 4000
NoConn ~ 7675 4475
Text Label 10300 5050 2    50   ~ 0
3V3-O250mA
Wire Wire Line
	9675 5050 10300 5050
Text Label 10300 5150 2    50   ~ 0
GND
Wire Wire Line
	9675 5150 10300 5150
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 5F7F2C2D
P 9475 5050
F 0 "J6" H 9750 5250 50  0000 C CNN
F 1 "3V3_1x2" H 9750 5175 50  0000 C CNN
F 2 "custom-footprints:UPDI_Header_1x02_P2.54mm_Horizontal" H 9475 5050 50  0001 C CNN
F 3 "~" H 9475 5050 50  0001 C CNN
	1    9475 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3700 6075 3700
Text Label 6075 3700 0    50   ~ 0
IO12-A11
$Comp
L B2B-PH-SM4-TB_LF__SN_:B2B-PH-SM4-TB(LF)(SN) J10
U 1 1 6071DF53
P 5600 4325
F 0 "J10" H 5525 4650 50  0000 L CNN
F 1 "BAT_B2B-PH" H 5350 4525 50  0000 L CNN
F 2 "custom-footprints:JST_B2B-PH-SM4-TB(LF)(SN)" H 5600 4325 50  0001 L BNN
F 3 "" H 5600 4325 50  0001 L BNN
F 4 "JST" H 5600 4325 50  0001 L BNN "MANUFACTURER"
	1    5600 4325
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6079F1DF
P 5100 4425
F 0 "R1" V 5300 4425 50  0000 C CNN
F 1 "0.536R_2W" V 5200 4425 50  0000 C CNN
F 2 "Resistor_SMD:R_2512_6332Metric_Pad1.52x3.35mm_HandSolder" V 5030 4425 50  0001 C CNN
F 3 "~" H 5100 4425 50  0001 C CNN
	1    5100 4425
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 4425 5250 4425
Text Notes 5100 5475 0    50   ~ 0
BAT pin:\n4.2 to 3.7V (LiPo) - 3V (Motor rated V) = (70 to 80mA/1000 * 18pcs) * R\nR = 0.49 to 0.56 (ohms)\nW = IV = (1.26 to 1.44A) * 0.7V = 0.882 to 1.008W
NoConn ~ 6075 3900
NoConn ~ 6075 4100
Text Label 5825 2800 2    50   ~ 0
3V3-O250mA
Wire Wire Line
	5200 2900 5825 2900
Wire Wire Line
	5200 2800 5825 2800
Text Label 5825 2900 2    50   ~ 0
GND
$Comp
L Connector:Conn_01x04_Female J8
U 1 1 5F82CFBE
P 5000 2900
F 0 "J8" H 4750 3225 50  0000 L CNN
F 1 "I2C_1x4" H 4625 3125 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical_SMD_Pin1Right" H 5000 2900 50  0001 C CNN
F 3 "~" H 5000 2900 50  0001 C CNN
	1    5000 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5200 3100 5825 3100
Wire Wire Line
	5200 3000 5825 3000
Text Label 5825 3100 2    50   ~ 0
IO23-SDA
Text Label 5825 3000 2    50   ~ 0
IO22-SCL
NoConn ~ 8975 2975
NoConn ~ 8975 2875
NoConn ~ 8975 2775
Text Label 8350 3075 0    50   ~ 0
IO4-A5-T2-CS
Wire Wire Line
	8975 3075 8350 3075
Text Label 8350 3375 0    50   ~ 0
IO5-SCK
Wire Wire Line
	8975 3375 8350 3375
Text Label 8350 3175 0    50   ~ 0
IO18-MOSI
Wire Wire Line
	8975 3175 8350 3175
Text Label 8350 3275 0    50   ~ 0
IO19-MISO
Wire Wire Line
	8975 3275 8350 3275
Wire Wire Line
	8975 3475 8350 3475
Text Label 8350 3475 0    50   ~ 0
GND
Wire Wire Line
	8975 3575 8350 3575
Text Label 8350 3575 0    50   ~ 0
3V3-O250mA
$Comp
L Connector:Conn_01x09_Female J3
U 1 1 6075E152
P 9175 3175
F 0 "J3" H 8875 3800 50  0000 L CNN
F 1 "micro_SD_01x09" H 8575 3700 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x09_P2.54mm_Vertical_SMD_Pin1Right" H 9175 3175 50  0001 C CNN
F 3 "~" H 9175 3175 50  0001 C CNN
	1    9175 3175
	1    0    0    -1  
$EndComp
Text Label 8125 850  3    50   ~ 0
IO21
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 607D9C6E
P 9125 3900
F 0 "J4" H 9550 4275 50  0000 C CNN
F 1 "Analog_IN_1x03" H 9600 4150 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical_SMD_Pin1Left" H 9125 3900 50  0001 C CNN
F 3 "~" H 9125 3900 50  0001 C CNN
	1    9125 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9325 3800 9875 3800
Text Label 9875 3900 2    50   ~ 0
I36-A4
Wire Wire Line
	9325 3900 9875 3900
Text Label 9875 3800 2    50   ~ 0
I39-A3
Wire Wire Line
	9325 4000 9875 4000
Text Label 9875 4000 2    50   ~ 0
I34-A2
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J5
U 1 1 60718C9D
P 6550 1750
F 0 "J5" V 6646 1462 50  0000 R CNN
F 1 "IDC_2x5_Palm_Back" V 6555 1462 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical_SMD" H 6550 1750 50  0001 C CNN
F 3 "~" H 6550 1750 50  0001 C CNN
	1    6550 1750
	0    -1   -1   0   
$EndComp
Text Label 6450 2500 1    50   ~ 0
Motor-BAT
Wire Wire Line
	6450 1950 6450 2500
Wire Wire Line
	6350 1950 6350 2500
Wire Wire Line
	6750 1950 6750 2500
Wire Wire Line
	6650 1950 6650 2500
Wire Wire Line
	6550 1950 6550 2500
Text Label 6750 2500 1    50   ~ 0
IO15-A8-T2
Text Label 6550 2500 1    50   ~ 0
IO23-SDA
Text Label 6650 2500 1    50   ~ 0
IO14-A6-T2
Text Label 6350 2500 1    50   ~ 0
GND
Text Label 6750 875  3    50   ~ 0
IO25-A1
Wire Wire Line
	6450 1450 6450 875 
Text Label 6650 875  3    50   ~ 0
IO12-A11
Wire Wire Line
	6750 1450 6750 875 
Text Label 6550 875  3    50   ~ 0
IO33-A9-T1
Wire Wire Line
	6350 1450 6350 875 
Text Label 6350 875  3    50   ~ 0
IO22-SCL
Wire Wire Line
	6550 1450 6550 875 
Text Label 6450 875  3    50   ~ 0
IO32-A7-T1
Wire Wire Line
	6650 1450 6650 875 
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J7
U 1 1 60716BE5
P 8025 1725
F 0 "J7" V 8121 1437 50  0000 R CNN
F 1 "IDC_2x5_Fingers" V 8030 1437 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical_SMD" H 8025 1725 50  0001 C CNN
F 3 "~" H 8025 1725 50  0001 C CNN
	1    8025 1725
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8225 1925 8225 2475
Text Label 7825 850  3    50   ~ 0
IO4-A5-T2-CS
Wire Wire Line
	8125 1925 8125 2475
Text Label 8125 2475 1    50   ~ 0
IO5-SCK
Wire Wire Line
	8025 1925 8025 2475
Text Label 7925 850  3    50   ~ 0
IO18-MOSI
Wire Wire Line
	7925 1925 7925 2475
Text Label 8225 2475 1    50   ~ 0
IO19-MISO
Wire Wire Line
	7825 1925 7825 2475
Text Label 8025 850  3    50   ~ 0
IO16-RX
Text Label 8225 850  3    50   ~ 0
IO17-TX
Text Label 8025 2475 1    50   ~ 0
IO26-A0
Wire Wire Line
	8225 1425 8225 850 
Wire Wire Line
	8025 1425 8025 850 
Text Label 7925 2475 1    50   ~ 0
IO13-A12-T2
Wire Wire Line
	8125 1425 8125 850 
Wire Wire Line
	7825 1425 7825 850 
Text Label 7825 2475 1    50   ~ 0
IO27-A10-T2
Wire Wire Line
	7925 1425 7925 850 
$EndSCHEMATC
